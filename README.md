# SimpleMind adds thinking to deep neural networks

### Under Construction!! Massive changes in the works ###
<!-- badges (build / coverage / pypi version) -->

[![pipeline status](https://gitlab.com/sm-ai-team/simplemind/badges/develop/pipeline.svg)](https://gitlab.com/sm-ai-team/simplemind/-/commits/develop)

<!-- ![SM_logo_v0](docs/figure/SIMPLEMIND_logo_multi_gradient_side_text_smaller.png) -->

<img src="docs/figure/SIMPLEMIND_logo_multi_gradient_side_text_smaller.png" alt="logo" width="80%"/>

SimpleMind is a Cognitive AI software environment that allows developers to add human knowledge and logical reasoning to deep neural networks.



## Table of Contents

- [Getting Started](#getting-started)
- [Running a Basic Example](#running-a-basic-example)
- [Explaining the Example](#explaining-the-example)
- [Creating Your Own Agent](#creating-your-own-agent)
- [Project Status](#project-status)
- [License](#license)
- [About Us](#about-us)

## Getting Started

This will contain installation instructions similar to SM Core.

## Running a Basic Example

We will provide an initial computer vision example for chest x-rays.

```
./sm run test-files/ping-pong.yaml
```

## Explaining the Example

SimpleMind enables easy implementation of complete Cognitive AI applications through:
1. networked aggregation of a large number of AI agents for different tasks;
2. a knowledge graph and machine reasoning to guide processing and cross-check agent results;
3. self organizing and tuning of agents using a genetic algorithm.


## Creating Your Own Agent

Try to find the ribs (or spine) - we need to provide annotated training data.



## Project Status

Under major renovation.

## License

SimpleMind is licensed under the 3-clause BSD license. For details, please see the LICENSE file.


## About Us

The SM core developers have built computer vision applications in medical imaging. But we have designed SimpleMind in the hope that the community will adapt it more broadly and expand its capabilities toward a more ambitious objective. 

The goal of the SimpleMind project is human-like thinking: to achieve human-level intelligence and then go beyond it.

*Mind: the element or complex of elements in an individual that feels, perceives, thinks, wills, and especially reasons.*
